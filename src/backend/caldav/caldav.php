<?php
/***********************************************
* File      :   caldav.php
* Project   :   PHP-Push
* Descr     :   This backend is based on 'BackendDiff' and implements a CalDAV interface
*
* Created   :   29.03.2012
*
* Copyright 2012 - 2014 Jean-Louis Dupond
*
* Jean-Louis Dupond released this code as AGPLv3 here: https://github.com/dupondje/PHP-Push-2/issues/93
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License, version 3,
* as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Consult LICENSE file for details
************************************************/

// config file
require_once("backend/caldav/config.php");

class BackendCalDAV extends BackendDiff {
    /**
     * @var CalDAVClient
     */
    private $_caldav;
    private $_caldav_path;

    private $changessinkinit;
    private $sinkdata;
    private $sinkmax;

    /**
    * Translation from Windows Timezone to Standard timezones
    */
    private const WINDOWSTOSTANDARDTZ = array(
        'AUS Central Standard Time' => 'Australia/Darwin',
        'AUS Eastern Standard Time' => 'Australia/Sydney',
        'Afghanistan Standard Time' => 'Asia/Kabul',
        'Alaskan Standard Time' => 'America/Anchorage',
        'Aleutian Standard Time' => 'America/Adak',
        'Altai Standard Time' => 'Asia/Barnaul',
        'Arab Standard Time' => 'Asia/Riyadh',
        'Arabian Standard Time' => 'Asia/Dubai',
        'Arabic Standard Time' => 'Asia/Baghdad',
        'Argentina Standard Time' => 'America/Buenos_Aires',
        'Astrakhan Standard Time' => 'Europe/Astrakhan',
        'Atlantic Standard Time' => 'America/Halifax',
        'Aus Central W. Standard Time' => 'Australia/Eucla',
        'Azerbaijan Standard Time' => 'Asia/Baku',
        'Azores Standard Time' => 'Atlantic/Azores',
        'Bahia Standard Time' => 'America/Bahia',
        'Bangladesh Standard Time' => 'Asia/Dhaka',
        'Belarus Standard Time' => 'Europe/Minsk',
        'Bougainville Standard Time' => 'Pacific/Bougainville',
        'Canada Central Standard Time' => 'America/Regina',
        'Cape Verde Standard Time' => 'Atlantic/Cape_Verde',
        'Caucasus Standard Time' => 'Asia/Yerevan',
        'Cen. Australia Standard Time' => 'Australia/Adelaide',
        'Central America Standard Time' => 'America/Guatemala',
        'Central Asia Standard Time' => 'Asia/Almaty',
        'Central Brazilian Standard Time' => 'America/Cuiaba',
        'Central Europe Standard Time' => 'Europe/Budapest',
        'Central European Standard Time' => 'Europe/Warsaw',
        'Central Pacific Standard Time' => 'Pacific/Guadalcanal',
        'Central Standard Time' => 'America/Chicago',
        'Central Standard Time (Mexico)' => 'America/Mexico_City',
        'Chatham Islands Standard Time' => 'Pacific/Chatham',
        'China Standard Time' => 'Asia/Shanghai',
        'Cuba Standard Time' => 'America/Havana',
        'E. Africa Standard Time' => 'Africa/Nairobi',
        'E. Australia Standard Time' => 'Australia/Brisbane',
        'E. Europe Standard Time' => 'Europe/Chisinau',
        'E. South America Standard Time' => 'America/Sao_Paulo',
        'Easter Island Standard Time' => 'Pacific/Easter',
        'Eastern Standard Time' => 'America/New_York',
        'Eastern Standard Time (Mexico)' => 'America/Cancun',
        'Egypt Standard Time' => 'Africa/Cairo',
        'Ekaterinburg Standard Time' => 'Asia/Yekaterinburg',
        'FLE Standard Time' => 'Europe/Kiev',
        'Fiji Standard Time' => 'Pacific/Fiji',
        'GMT Standard Time' => 'Europe/London',
        'GTB Standard Time' => 'Europe/Bucharest',
        'Georgian Standard Time' => 'Asia/Tbilisi',
        'Greenland Standard Time' => 'America/Godthab',
        'Greenwich Standard Time' => 'Atlantic/Reykjavik',
        'Haiti Standard Time' => 'America/Port-au-Prince',
        'Hawaiian Standard Time' => 'Pacific/Honolulu',
        'India Standard Time' => 'Asia/Calcutta',
        'Iran Standard Time' => 'Asia/Tehran',
        'Israel Standard Time' => 'Asia/Jerusalem',
        'Jordan Standard Time' => 'Asia/Amman',
        'Kaliningrad Standard Time' => 'Europe/Kaliningrad',
        'Korea Standard Time' => 'Asia/Seoul',
        'Libya Standard Time' => 'Africa/Tripoli',
        'Line Islands Standard Time' => 'Pacific/Kiritimati',
        'Lord Howe Standard Time' => 'Australia/Lord_Howe',
        'Magadan Standard Time' => 'Asia/Magadan',
        'Magallanes Standard Time' => 'America/Punta_Arenas',
        'Marquesas Standard Time' => 'Pacific/Marquesas',
        'Mauritius Standard Time' => 'Indian/Mauritius',
        'Middle East Standard Time' => 'Asia/Beirut',
        'Montevideo Standard Time' => 'America/Montevideo',
        'Morocco Standard Time' => 'Africa/Casablanca',
        'Mountain Standard Time' => 'America/Denver',
        'Mountain Standard Time (Mexico)' => 'America/Chihuahua',
        'Myanmar Standard Time' => 'Asia/Rangoon',
        'N. Central Asia Standard Time' => 'Asia/Novosibirsk',
        'Namibia Standard Time' => 'Africa/Windhoek',
        'Nepal Standard Time' => 'Asia/Katmandu',
        'New Zealand Standard Time' => 'Pacific/Auckland',
        'Newfoundland Standard Time' => 'America/St_Johns',
        'Norfolk Standard Time' => 'Pacific/Norfolk',
        'North Asia East Standard Time' => 'Asia/Irkutsk',
        'North Asia Standard Time' => 'Asia/Krasnoyarsk',
        'North Korea Standard Time' => 'Asia/Pyongyang',
        'Omsk Standard Time' => 'Asia/Omsk',
        'Pacific SA Standard Time' => 'America/Santiago',
        'Pacific Standard Time' => 'America/Los_Angeles',
        'Pacific Standard Time (Mexico)' => 'America/Tijuana',
        'Pakistan Standard Time' => 'Asia/Karachi',
        'Paraguay Standard Time' => 'America/Asuncion',
        'Qyzylorda Standard Time' => 'Asia/Qyzylorda',
        'Romance Standard Time' => 'Europe/Paris',
        'Russia Time Zone 10' => 'Asia/Srednekolymsk',
        'Russia Time Zone 11' => 'Asia/Kamchatka',
        'Russia Time Zone 3' => 'Europe/Samara',
        'Russian Standard Time' => 'Europe/Moscow',
        'SA Eastern Standard Time' => 'America/Cayenne',
        'SA Pacific Standard Time' => 'America/Bogota',
        'SA Western Standard Time' => 'America/La_Paz',
        'SE Asia Standard Time' => 'Asia/Bangkok',
        'Saint Pierre Standard Time' => 'America/Miquelon',
        'Sakhalin Standard Time' => 'Asia/Sakhalin',
        'Samoa Standard Time' => 'Pacific/Apia',
        'Sao Tome Standard Time' => 'Africa/Sao_Tome',
        'Saratov Standard Time' => 'Europe/Saratov',
        'Singapore Standard Time' => 'Asia/Singapore',
        'South Africa Standard Time' => 'Africa/Johannesburg',
        'Sri Lanka Standard Time' => 'Asia/Colombo',
        'Sudan Standard Time' => 'Africa/Khartoum',
        'Syria Standard Time' => 'Asia/Damascus',
        'Taipei Standard Time' => 'Asia/Taipei',
        'Tasmania Standard Time' => 'Australia/Hobart',
        'Tocantins Standard Time' => 'America/Araguaina',
        'Tokyo Standard Time' => 'Asia/Tokyo',
        'Tomsk Standard Time' => 'Asia/Tomsk',
        'Tonga Standard Time' => 'Pacific/Tongatapu',
        'Transbaikal Standard Time' => 'Asia/Chita',
        'Turkey Standard Time' => 'Europe/Istanbul',
        'Turks And Caicos Standard Time' => 'America/Grand_Turk',
        'US Eastern Standard Time' => 'America/Indianapolis',
        'US Mountain Standard Time' => 'America/Phoenix',
        'UTC' => 'America/Danmarkshavn',
        'UTC+12' => 'Pacific/Tarawa',
        'UTC+13' => 'Pacific/Enderbury',
        'UTC-02' => 'America/Noronha',
        'UTC-08' => 'Pacific/Pitcairn',
        'UTC-09' => 'Pacific/Gambier',
        'UTC-11' => 'Pacific/Pago_Pago',
        'Ulaanbaatar Standard Time' => 'Asia/Ulaanbaatar',
        'Venezuela Standard Time' => 'America/Caracas',
        'Vladivostok Standard Time' => 'Asia/Vladivostok',
        'Volgograd Standard Time' => 'Europe/Volgograd',
        'W. Australia Standard Time' => 'Australia/Perth',
        'W. Central Africa Standard Time' => 'Africa/Lagos',
        'W. Europe Standard Time' => 'Europe/Berlin',
        'W. Mongolia Standard Time' => 'Asia/Hovd',
        'West Asia Standard Time' => 'Asia/Tashkent',
        'West Bank Standard Time' => 'Asia/Hebron',
        'West Pacific Standard Time' => 'Pacific/Port_Moresby',
        'Yakutsk Standard Time' => 'Asia/Yakutsk',
        'Yukon Standard Time' => 'America/Whitehorse',
    );

    /**
     * Constructor
     */
    public function __construct() {
        if (!function_exists("curl_init")) {
            throw new FatalException("BackendCalDAV(): php-curl is not found", 0, null, LOGLEVEL_FATAL);
        }

        $this->changessinkinit = false;
        $this->sinkdata = array();
        $this->sinkmax = array();
    }

    /**
     * Login to the CalDAV backend
     * @see IBackend::Logon()
     */
    public function Logon($username, $domain, $password) {
        $this->_caldav_path = str_replace('%u', $username, CALDAV_PATH);
        $url = sprintf("%s://%s:%d%s", CALDAV_PROTOCOL, CALDAV_SERVER, CALDAV_PORT, $this->_caldav_path);
        $this->_caldav = new CalDAVClient($url, $username, $password);
        if ($connected = $this->_caldav->CheckConnection()) {
            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->Logon(): User '%s' is authenticated on CalDAV '%s'", $username, $url));
            if ($domain == "") {
                $this->originalUsername = $username;
            }
            else {
                $this->originalUsername = $username . '@' . $domain;
            }
        }
        else {
            ZLog::Write(LOGLEVEL_WARN, sprintf("BackendCalDAV->Logon(): User '%s' is not authenticated on CalDAV '%s'", $username, $url));
        }

        return $connected;
    }

    /**
     * The connections to CalDAV are always directly closed. So nothing special needs to happen here.
     * @see IBackend::Logoff()
     */
    public function Logoff() {
        if ($this->_caldav != null) {
            $this->_caldav->Disconnect();
            unset($this->_caldav);
        }

        $this->SaveStorages();

        unset($this->sinkdata);
        unset($this->sinkmax);

        ZLog::Write(LOGLEVEL_DEBUG, "BackendCalDAV->Logoff(): disconnected from CALDAV server");

        return true;
    }

    /**
     * CalDAV doesn't need to handle SendMail
     * @see IBackend::SendMail()
     */
    public function SendMail($sm) {
        return false;
    }

    /**
     * No attachments in CalDAV
     * @see IBackend::GetAttachmentData()
     */
    public function GetAttachmentData($attname) {
        return false;
    }

    /**
     * Deletes are always permanent deletes. Messages doesn't get moved.
     * @see IBackend::GetWasteBasket()
     */
    public function GetWasteBasket() {
        return false;
    }

    /**
     * Processes a response to a meeting request.
     * CalendarID is a reference and has to be set if a new calendar item is created
     * @see BackendDiff::MeetingResponse()
     */
    public function MeetingResponse($requestid, $folderid, $response) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->MeetingResponse('%s','%s','%s')", $requestid, $folderid, $response));
        if ($folderid[0] != "C")
            throw new StatusException("BackendCalDAV->MeetingResponse(): Folder is not a calendar!", SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
        $message = $this->GetMessageByID($folderid, $requestid);
        if ($message == null)
            throw new StatusException("BackendCalDAV->MeetingResponse(): Error, event not found", SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
        $userDetails = ZPush::GetBackend()->GetCurrentUsername();
        $vcal = Sabre\VObject\Reader::read($message['data'], Sabre\VObject\Reader::OPTION_FORGIVING);
        foreach ($vcal->VEVENT as $vevent) {
            foreach ($vevent->children() as $property) {
                if ($property->name != "ATTENDEE")
                    continue;
                $att_email = str_ireplace("MAILTO:", "", (string)$property);
                if ($att_email == $userDetails['emailaddress']) {
                    switch($response) {
                        case 1: // Accepted
                            $property['PARTSTAT'] = "ACCEPTED";
                            break;
                        case 2: // Tentatively accepted
                            $property['PARTSTAT'] = "TENTATIVE";
                            break;
                        case 3: // Declined
                            $property['PARTSTAT'] = "DECLINED";
                            break;
                    }
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->MeetingResponse(): Updated participation to %s", $property['PARTSTAT']));
                }
            }
        }
        $url = $this->_caldav_path . substr($folderid, 1) . "/" . $requestid;
        $this->CreateUpdateCalendar($vcal->serialize(), $url, $message['etag']);
        return $requestid;
    }

    /**
     * Get a list of all the folders we are going to sync.
     * Each caldav calendar can contain tasks (prefix T) and events (prefix C), so duplicate each calendar found.
     * @see BackendDiff::GetFolderList()
     */
    public function GetFolderList() {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->GetFolderList(): Getting all folders."));
        $folders = array();
        $calendars = $this->_caldav->FindCalendars();
        foreach ($calendars as $val) {
            $fpath = explode("/", $val->url, -1);
            if (is_array($fpath)) {
                $folderid = array_pop($fpath);
                if ($val->supported_comp == null) {
                    $id = "C" . $folderid;
                    $folders[] = $this->StatFolder($id);
                    $id = "T" . $folderid;
                    $folders[] = $this->StatFolder($id);
                } else {
                    foreach ($val->supported_comp as $comp) {
                        switch($comp) {
                            case 'VEVENT':
                                $id = "C" . $folderid;
                                $folders[] = $this->StatFolder($id);
                                break;
                            case 'VTODO':
                                $id = "T" . $folderid;
                                $folders[] = $this->StatFolder($id);
                                break;
                        }
                    }
                }
            }
        }
        return $folders;
    }

    /**
     * Returning a SyncFolder
     * @see BackendDiff::GetFolder()
     */
    public function GetFolder($id) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->GetFolder('%s')", $id));
        $val = $this->_caldav->GetCalendarDetails($this->_caldav_path . substr($id, 1) .  "/");
        $folder = new SyncFolder();
        $folder->parentid = 0;
        $folder->displayname = $val->displayname;
        $folder->serverid = $id;
        if ($id[0] == "C") {
            if (defined('CALDAV_PERSONAL') && strcasecmp(substr($id, 1), CALDAV_PERSONAL) == 0) {
                $folder->type = SYNC_FOLDER_TYPE_APPOINTMENT;
            }
            else {
                $folder->type = SYNC_FOLDER_TYPE_USER_APPOINTMENT;
            }
        }
        else {
            if (defined('CALDAV_PERSONAL') && strcasecmp(substr($id, 1), CALDAV_PERSONAL) == 0) {
                $folder->type = SYNC_FOLDER_TYPE_TASK;
            }
            else {
                $folder->type = SYNC_FOLDER_TYPE_USER_TASK;
            }
        }
        return $folder;
    }

    /**
     * Returns information on the folder.
     * @see BackendDiff::StatFolder()
     */
    public function StatFolder($id) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->StatFolder('%s')", $id));
        $val = $this->GetFolder($id);
        $folder = array();
        $folder["id"] = $id;
        $folder["parent"] = $val->parentid;
        $folder["mod"] = $val->serverid;
        return $folder;
    }

    /**
     * ChangeFolder is not supported under CalDAV
     * @see BackendDiff::ChangeFolder()
     */
    public function ChangeFolder($folderid, $oldid, $displayname, $type) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangeFolder('%s','%s','%s','%s')", $folderid, $oldid, $displayname, $type));
        return false;
    }

    /**
     * DeleteFolder is not supported under CalDAV
     * @see BackendDiff::DeleteFolder()
     */
    public function DeleteFolder($id, $parentid) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->DeleteFolder('%s','%s')", $id, $parentid));
        return false;
    }

    /**
    * Format State message
    */
    private function FormatState($data) {
        $message = array();
        $message['id'] = $data['href'];
        $message['flags'] = 1;
        $message['mod'] = $data['etag'];
        return $message;
    }

    /**
     * Get a list of all the messages.
     * @see BackendDiff::GetMessageList()
     */
    public function GetMessageList($folderid, $cutoffdate) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->GetMessageList('%s','%s')", $folderid, $cutoffdate));

        /* Calculating the range of events we want to sync */
        $begin = gmdate("Ymd\THis\Z", $cutoffdate);
        $finish = gmdate("Ymd\THis\Z", CALDAV_MAX_SYNC_PERIOD);

        $path = $this->_caldav_path . substr($folderid, 1) . "/";
        if ($folderid[0] == "C") {
            $msgs = $this->_caldav->GetEventsList($begin, $finish, $path);
        }
        else {
            $msgs = $this->_caldav->GetTodosList($begin, $finish, false, false, $path);
        }

        $messages = array();
        foreach ($msgs as $e) {
            $messages[] = $this->FormatState($e);
        }
        return $messages;
    }

    /**
    * Get a Message by its ID
    */
    private function GetMessageByID($folderid, $id) {
        $path = $this->_caldav_path . substr($folderid, 1) . "/";
        $href = $path . $id;
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->GetMessageByID Querying server for message with href '%s' in folder '%s'", $href, $path));
        $data = $this->_caldav->GetEntryByHref($href);
        return $data;
    }

    /**
     * Get a SyncObject by its ID
     * @see BackendDiff::GetMessage()
     */
    public function GetMessage($folderid, $id, $contentparameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->GetMessage('%s','%s')", $folderid,  $id));
        $message = $this->GetMessageByID($folderid, $id);
        if ($message == null || $message['data'] == '') {
            //we don't have any ical information, return false so that the diff backend raises an exception
            return false;
        }
        $data = $message['data'];
        if ($folderid[0] == "C") {
            return $this->_ParseVEventToAS($data, $contentparameters);
        }
        if ($folderid[0] == "T") {
            return $this->_ParseVTodoToAS($data, $contentparameters);
        }
        return false;
    }

    /**
     * Return id, flags and mod of a messageid
     * @see BackendDiff::StatMessage()
     */
    public function StatMessage($folderid, $id) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->StatMessage('%s','%s')", $folderid,  $id));
        $data = $this->GetMessageByID($folderid, $id);
        if ($data== null) {
            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->StatMessage No item on server with uid '%s' in folder '%s'", substr($id, 0, strlen($id)-4), $path));
            return;
        }
        return $this->FormatState($data);
    }

    /**
     * Change/Add a message with contents received from ActiveSync
     * @see BackendDiff::ChangeMessage()
     */
    public function ChangeMessage($folderid, $id, $message, $contentParameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangeMessage('%s','%s')", $folderid,  $id));

        $version = 0;
        if ($id) {
            $existing = $this->GetMessageByID($folderid, $id);
            if ($existing == null) {
                $id = null;
            } else {
                $etag = $existing['etag'];
                $uid = substr($id, 0, strlen($id)-4);
                if ($folderid[0] == "C") {
                    $vcal = Sabre\VObject\Reader::read($existing['data'], Sabre\VObject\Reader::OPTION_FORGIVING);
                    foreach ($vcal->VEVENT as $vevent) {
                        foreach ($vevent->children() as $property) {
                            if ($property->name == "UID") {
                                if ($uid != (string)$property->getValue()) {
                                    $version = 0;
                                    break;
                                }
                            }
                            elseif ($property->name == "SEQUENCE") {
                                $version = $property->getValue();
                            }
                        };
                        if ($version != 0)
                            break;
                    }
                }
            }
        }
        if (!$id) {
            $etag = "*";
            if (!empty($message->uid)) {
                $id = sprintf("%s.ics", $message->uid);
            } else {
                $id = sprintf("%s-%s.ics", gmdate("Ymd\THis\Z"), hash("md5", microtime()));
            }
        }

        $url = $this->_caldav_path . substr($folderid, 1) . "/" . $id;

        $data = $this->_ParseASToVCalendar($message, $folderid, substr($id, 0, strlen($id) - 4), $version);

        $etag_new = $this->CreateUpdateCalendar($data, $url, $etag);

        $item = array();
        $item['href'] = $id;
        $item['etag'] = $etag_new;
        $item['data'] = $data;

        return $this->FormatState($item);
    }

    /**
     * Change the read flag is not supported.
     * @see BackendDiff::SetReadFlag()
     */
    public function SetReadFlag($folderid, $id, $flags, $contentParameters) {
        return false;
    }

    /**
     * Delete a message from the CalDAV server.
     * @see BackendDiff::DeleteMessage()
     */
    public function DeleteMessage($folderid, $id, $contentParameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->DeleteMessage('%s','%s')", $folderid,  $id));
        $url = $this->_caldav_path . substr($folderid, 1) . "/" . $id;
        $http_status_code = $this->_caldav->DoDELETERequest($url);
        return $http_status_code == "204";
    }

    /**
     * Move a message is not supported by CalDAV.
     * @see BackendDiff::MoveMessage()
     */
    public function MoveMessage($folderid, $id, $newfolderid, $contentParameters) {
        return false;
    }

    /**
     * Create or Update one event
     *
     * @access public
     * @param $data     string      VCALENDAR text
     * @param $url      string      URL for the calendar, if false a new calendar object is created
     * @param $etag     string      ETAG for the calendar, if '*' is a new object
     * @return array
     */
    public function CreateUpdateCalendar($data, $url = false, $etag = "*") {
        if ($url === false) {
            $url = sprintf("%s%s/%s-%s.ics", $this->_caldav_path, CALDAV_PERSONAL, gmdate("Ymd\THis\Z"), hash("md5", microtime()));
            $etag = "*";
        }

        return $this->_caldav->DoPUTRequest($url, $data, $etag);
    }

    /**
     * Deletes one VCALENDAR
     *
     * @access public
     * @param $id       string      ID of the VCALENDAR
     * @return boolean
     */
    public function DeleteCalendar($id) {
        $http_status_code = $this->_caldav->DoDELETERequest(sprintf("%s%s/%s", $this->_caldav_path, CALDAV_PERSONAL, $id));
        return $http_status_code == "204";
    }

    /**
     * Finds one VCALENDAR
     *
     * @access public
     * @param $uid      string      UID attribute
     * @return array
     */
    public function FindCalendar($uid) {
        $filter = sprintf("<C:filter><C:comp-filter name=\"VCALENDAR\"><C:comp-filter name=\"VEVENT\"><C:prop-filter name=\"UID\"><C:text-match>%s</C:text-match></C:prop-filter></C:comp-filter></C:comp-filter></C:filter>", $uid);

        $events = $this->_caldav->DoCalendarQuery($filter, sprintf("%s%s", $this->_caldav_path, CALDAV_PERSONAL));

        return $events;
    }

    /**
     * Resolves recipients
     *
     * @param SyncObject        $resolveRecipients
     *
     * @access public
     * @return SyncObject       $resolveRecipients
     */
    public function ResolveRecipients($resolveRecipients) {
        // TODO:
        return false;
    }

    /**
     * Indicates which AS version is supported by the backend.
     *
     * @access public
     * @return string       AS version constant
     */
    public function GetSupportedASVersion() {
        return ZPush::ASV_14;
    }

    /**
     * Indicates if the backend has a ChangesSink.
     * A sink is an active notification mechanism which does not need polling.
     * The CalDAV backend simulates a sink by polling revision dates from the events or use the native sync-collection.
     *
     * @access public
     * @return boolean
     */
    public function HasChangesSink() {
        return true;
    }

    /**
     * The folder should be considered by the sink.
     * Folders which were not initialized should not result in a notification
     * of IBackend->ChangesSink().
     *
     * @param string        $folderid
     *
     * @access public
     * @return boolean      false if found can not be found
     */
    public function ChangesSinkInitialize($folderid) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangesSinkInitialize(): folderid '%s'", $folderid));

        // We don't need the actual events, we only need to get the changes since this moment
        $init_ok = true;
        $url = $this->_caldav_path . substr($folderid, 1) . "/";
        $this->sinkdata[$folderid] = $this->_caldav->GetSync($url, true, CALDAV_SUPPORTS_SYNC);
        if (CALDAV_SUPPORTS_SYNC) {
            // we don't need to store the sinkdata if the caldav server supports native sync
            unset($this->sinkdata[$url]);
            $this->sinkdata[$folderid] = array();
        }

        $this->changessinkinit = $init_ok;
        $this->sinkmax = array();

        return $this->changessinkinit;
    }

    /**
     * The actual ChangesSink.
     * For max. the $timeout value this method should block and if no changes
     * are available return an empty array.
     * If changes are available a list of folderids is expected.
     *
     * @param int           $timeout        max. amount of seconds to block
     *
     * @access public
     * @return array
     */
    public function ChangesSink($timeout = 30) {
        $notifications = array();
        $stopat = time() + $timeout - 1;

        //We can get here and the ChangesSink not be initialized yet
        if (!$this->changessinkinit) {
            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangesSink - Not initialized ChangesSink, sleep and exit"));
            // We sleep and do nothing else
            sleep($timeout);
            return $notifications;
        }

        // only check once to reduce pressure in the DAV server
        foreach ($this->sinkdata as $k => $v) {
            $changed = false;

            $url = $this->_caldav_path . substr($k, 1) . "/";
            $response = $this->_caldav->GetSync($url, false, CALDAV_SUPPORTS_SYNC);

            if (CALDAV_SUPPORTS_SYNC) {
                if (count($response) > 0) {
                    $changed = true;
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangesSink - Changes detected"));
                }
            }
            else {
                // If the numbers of events are different, we know for sure, there are changes
                if (count($response) != count($v)) {
                    $changed = true;
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangesSink - Changes detected"));
                }
                else {
                    // If the numbers of events are equals, we compare the biggest date
                    // FIXME: we are comparing strings no dates
                    if (!isset($this->sinkmax[$k])) {
                        $this->sinkmax[$k] = '';
                        for ($i = 0; $i < count($v); $i++) {
                            if ($v[$i]['getlastmodified'] > $this->sinkmax[$k]) {
                                $this->sinkmax[$k] = $v[$i]['getlastmodified'];
                            }
                        }
                    }

                    for ($i = 0; $i < count($response); $i++) {
                        if ($response[$i]['getlastmodified'] > $this->sinkmax[$k]) {
                            $changed = true;
                        }
                    }

                    if ($changed) {
                        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->ChangesSink - Changes detected"));
                    }
                }
            }

            if ($changed) {
                $notifications[] = $k;
            }
        }

        // Wait to timeout
        if (empty($notifications)) {
            while ($stopat > time()) {
                sleep(1);
            }
        }

        return $notifications;
    }


    /**
     * Convert a iCAL VEvent to ActiveSync format
     * @param ical_vevent $data
     * @param ContentParameters $contentparameters
     * @return SyncAppointment
     */
    private function _ParseVEventToAS($data, $contentparameters) {
        ZLog::Write(LOGLEVEL_DEBUG, "BackendCalDAV->_ParseVEventToAS(): Parsing VEvent");

        $truncsize = Utils::GetTruncSize($contentparameters->GetTruncation());
        $message = new SyncAppointment();

        $vcal = Sabre\VObject\Reader::read($data, Sabre\VObject\Reader::OPTION_FORGIVING);
        ZLog::Write(LOGLEVEL_WBXML, "VObject vcal: ".print_r($vcal->serialize(), 1));

        foreach ($vcal->VEVENT as $vevent) {
            if (isset($vevent->{'RECURRENCE-ID'})) {
                try {
                    $exceptionstarttime = $vevent->{'RECURRENCE-ID'}->getDateTime()->getTimestamp();
                    $exception = new SyncAppointmentException();
                    $exception->exceptionstarttime = $exceptionstarttime;
                    $exception->deleted = 0;
                    $exception = $this->_ParseVEventToSyncObject($vevent, $exception, $truncsize);
                    unset($exception->recurrence);
                    if (!isset($message->exceptions)) {
                        $message->exceptions = array();
                    }
                    $message->exceptions[] = $exception;
                }
                catch (Exception $e) {
                    ZLog::Write(LOGLEVEL_ERROR, sprintf("BackendCalDAV->_ParseVEventToSyncObject(): Exception during processing RECURRENCE-ID: '%s'", $e->getMessage()));
                }
            }
            else {
                $message = $this->_ParseVEventToSyncObject($vevent, $message, $truncsize);
            }
        }
        // Loop exceptions and set timezone
        if (isset($message->exceptions)) {
            foreach ($message->exceptions as $exception) {
                $exception->timezone = $message->timezone;
            }
        }

        return $message;
    }

    /**
     * Parse 1 VEvent
     * @param Sabre/VEvent $vevent
     * @param SyncAppointment(Exception) $message
     * @param int $truncsize
     */
    private function _ParseVEventToSyncObject($vevent, $message, $truncsize) {
        //Defaults
        $message->busystatus = 2;
        $message->meetingstatus = 0;
        $message->alldayevent = 0;
        $tzid = defined('TIMEZONE') && strlen(TIMEZONE) > 1 ? TIMEZONE : date_default_timezone_get();

        foreach ($vevent->children() as $property) {
            switch ($property->name) {
                case "DTSTART":
                    $message->starttime = $property->getDateTime()->getTimestamp();
                    if (isset($property['TZID'])) {
                        $tzid = $property->getDateTime()->getTimezone()->getName();
                    }
                    else if (((string)$property->getValue())[-1] == "Z") {
                        $tzid = 'UTC';
                    }
                    else {
                        $timezone = new DateTimeZone($tzid);
                        $message->starttime = $message->starttime - $timezone->getOffset($property->getDateTime());
                    }
                    if (strlen((string)$property) == 8) {
                        $message->alldayevent = 1;
                    }
                    break;

                case "DTEND":
                    $message->endtime = $property->getDateTime()->getTimestamp();
                    if ((!isset($property['TZID'])) && (((string)$property->getValue())[-1] != "Z")) {
                        $timezone = new DateTimeZone($tzid);
                        $message->endtime = $message->endtime - $timezone->getOffset($property->getDateTime());
                    }
                    if (strlen((string)$property) == 8) {
                        $message->alldayevent = 1;
                    }
                    break;

                case "DURATION":
                    if (!isset($message->endtime)) {
                        $start = date_create("@" . $message->starttime);
                        $val = str_replace("+", "", (string)$property);
                        $interval = new DateInterval($val);
                        $message->endtime = date_timestamp_get(date_add($start, $interval));
                    }
                    break;

                case "X-MICROSOFT-CDO-ALLDAYEVENT":
                    if ((string)$property == "TRUE") {
                        $message->alldayevent = 1;
                    }
                    break;

                case "SUMMARY":
                    $message->subject = (string)$property;
                    break;

                case "LOCATION":
                    $message->location = (string)$property;
                    break;

                case "DESCRIPTION":
                    if (Request::GetProtocolVersion() >= 12.0) {
                        $message->asbody = new SyncBaseBody();

                        // the DESCRIPTION component is specified to be plain text (RFC5545), for HTML use X-ALT-DESC
                        $data = str_replace("\n","\r\n", str_replace("\r","",Utils::ConvertHtmlToText((string)$property)));

                        // truncate body, if requested
                        if (strlen($data) > $truncsize) {
                            $message->asbody->truncated = 1;
                            $data = Utils::Utf8_truncate($data, $truncsize);
                        }
                        else {
                            $message->asbody->truncated = 0;
                        }
                        $message->asbody->data = StringStreamWrapper::Open($data);
                        $message->asbody->estimatedDataSize = strlen($data);
                        unset($data);

                        // set body type accordingly
                        $message->asbody->type = SYNC_BODYPREFERENCE_PLAIN;
                        $message->nativebodytype = SYNC_BODYPREFERENCE_PLAIN;
                    }
                    else {
                        $body = $property->Value();
                        // truncate body, if requested
                        if(strlen($body) > $truncsize) {
                            $body = Utils::Utf8_truncate($body, $truncsize);
                            $message->bodytruncated = 1;
                        } else {
                            $message->bodytruncated = 0;
                        }
                        $body = str_replace("\n","\r\n", str_replace("\r","",$body));
                        $message->body = $body;
                    }
                    break;

                case "RRULE":
                    $message->recurrence = $this->_ParseRecurrence((string)$property, "vevent");
                    break;

                case "CLASS":
                    switch ((string)$property) {
                        case "PUBLIC":
                            $message->sensitivity = 0;
                            break;
                        case "PRIVATE":
                            $message->sensitivity = 2;
                            break;
                        case "CONFIDENTIAL":
                            $message->sensitivity = 3;
                            break;
                    }
                    break;

                case "TRANSP":
                    if(!isset($message->busystatus)){
                        switch ((string)$property) {
                            case "TRANSPARENT":
                                $message->busystatus = 0;
                                break;
                            case "OPAQUE":
                                $message->busystatus = 2;
                                break;
                        }
                    }
                    break;

                case "X-MICROSOFT-CDO-INTENDEDSTATUS":
                    switch ((string)$property) {
                        case "FREE":
                            $message->busystatus = 0;
                            break;
                        case "TENTATIVE":
                            $message->busystatus = 1;
                            break;
                        case "BUSY":
                            $message->busystatus = 2;
                            break;
                        case "OOF":
                            $message->busystatus = 3;
                            break;
                        case "WORKINGELSEWHERE":
                            $message->busystatus = 4;
                            break;
                    }
                    break;

                case "STATUS":
                    switch ((string)$property) {
                        case "TENTATIVE":
                            $message->meetingstatus = 3; // Meeting received
                            break;
                        case "CONFIRMED":
                            $message->meetingstatus = 1; // is a meeting
                            break;
                        case "CANCELLED":
                            $message->meetingstatus = 5; // Meeting is canceled
                            break;
                    }
                    break;

                case "ORGANIZER":
                    $org_mail = str_ireplace("MAILTO:", "", (string)$property);
                    $message->organizeremail = $org_mail;
                    if (isset($property['CN'])) {
                        $message->organizername = (string)$property['CN'];
                    }
                    else {
                        $message->organizername = "";
                    }
                    break;

                case "ATTENDEE":
                    $attendee = new SyncAttendee();
                    $att_email = str_ireplace("MAILTO:", "", (string)$property);
                    $attendee->email = $att_email;
                    if (isset($property['CN'])) {
                        $attendee->name = (string)$property['CN'];
                    }
                    else {
                        $attendee->name = "";
                    }
                    if (isset($property['PARTSTAT'])) {
                        switch($property['PARTSTAT']){
                            case "TENTATIVE":
                                $attendee->attendeestatus = 2;
                                break;
                            case "ACCEPTED":
                                $attendee->attendeestatus = 3;
                                break;
                            case "DECLINED":
                                $attendee->attendeestatus = 4;
                                break;
                            case "NEEDS-ACTION":
                                $attendee->attendeestatus = 5;
                                break;
                        }
                    }
                    if (isset($property['ROLE'])) {
                        switch($property['ROLE']){
                            case "OPT-PARTICIPANT":
                                $attendee->attendeetype = 2; // Optional
                                break;
                            case "NON-PARTICIPANT":
                                $attendee->attendeetype = 3; // Resource
                                break;
                            case "CHAIR":
                            case "REQ-PARTICIPANT":
                            default:
                                $attendee->attendeetype = 1; // Required
                                break;
                        }
                    }
                    if (isset($message->attendees) && is_array($message->attendees)) {
                        $message->attendees[] = $attendee;
                    }
                    else {
                        $message->attendees = array($attendee);
                    }
                    break;

                case "CATEGORIES":
                    $categories = explode(",", (string)$property);
                    $message->categories = $categories;
                    break;

                case "EXDATE":
                    try {
                        $exceptionstarttime = $property->getDateTime()->getTimestamp();
                        $exception = new SyncAppointmentException();
                        $exception->deleted = 1;
                        $exception->exceptionstarttime = $exceptionstarttime;
                        if (!isset($message->exceptions)) {
                            $message->exceptions = array();
                        }
                        $message->exceptions[] = $exception;
                    }
                    catch (Exception $e) {
                        ZLog::Write(LOGLEVEL_ERROR, sprintf("BackendCalDAV->_ParseVEventToSyncObject(): Exception during processing EXDATE: '%s'", $e->getMessage()));
                    }
                    break;

                case "UID":
                    $message->uid = (string)$property;
                    break;

                case "LAST-MODIFIED":
                    try {
                        $message->dtstamp = $property->getDateTime()->getTimestamp();
                    }
                    catch (Exception $e) {
                        ZLog::Write(LOGLEVEL_ERROR, sprintf("BackendCalDAV->_ParseVEventToSyncObject(): Exception during processing LAST-MODIFIED: '%s'", $e->getMessage()));
                    }
                    break;

                case "VALARM":
                    foreach($property->children() as $valarm_child) {
                        if($valarm_child->name == "TRIGGER") {
                            if(isset($valarm_child->parameters['VALUE']) && $valarm_child->parameters['VALUE'] == 'DATE-TIME') {
                                // the trigger is absolute
                                $begin = date_create("@" . $vevent->DTSTART->getDateTime()->getTimestamp());
                                if (!$begin) {
                                    ZLog::Write(LOGLEVEL_ERROR, "BackendCalDAV->_ParseVEventToSyncObject(): Error during processing DTSTART for VALARM");
                                    break;
                                }
                                $trigger = date_create("@" . TimezoneUtil::MakeUTCDate((string)$valarm_child));
                                if (!$trigger) {
                                    ZLog::Write(LOGLEVEL_ERROR, "BackendCalDAV->_ParseVEventToSyncObject(): Error during processing TRIGGER for VALARM");
                                    break;
                                }
                                $interval = date_diff($begin, $trigger);
                                $message->reminder = $interval->format("%i") + $interval->format("%h") * 60 + $interval->format("%a") * 60 * 24;
                            }
                            else {
                                // the trigger is relative
                                // ICAL allows the trigger to be relative to something other than the start, AS does not support this
                                $val = str_replace("-", "", (string)$valarm_child);
                                try{
                                    $interval = new DateInterval($val); // Crash here
                                }catch(Exception $e){
                                    ZLog::Write(LOGLEVEL_ERROR, sprintf("BackendCalDAV->_ParseVEventToSyncObject(): Exception during processing VALARM TRIGGER: '%s'", $e->getMessage()));
                                    break;
                                }
                                $message->reminder = intval($interval->format("%i")) + intval($interval->format("%h")) * 60 + intval($interval->format("%a")) * 60 * 24;
                            }
                        }
                    }
                    break;

                //We can ignore the following
                case "PRIORITY":
                case "SEQUENCE":
                case "CREATED":
                case "DTSTAMP":
                case "X-MOZ-GENERATION":
                case "X-MOZ-LASTACK":
                case "X-LIC-ERROR":
                case "RECURRENCE-ID":
                case "X-MICROSOFT-CDO-BUSYSTATUS":
                case "X-MICROSOFT-DISALLOW-COUNTER":
                case "X-MICROSOFT-CDO-OWNER-CRITICAL-CHANGE":
                case "X-MICROSOFT-CDO-ATTENDEE-CRITICAL-CHANGE":
                case "X-MICROSOFT-CDO-APPT-SEQUENCE":
                case "X-MICROSOFT-CDO-OWNERAPPTID":
                case "X-ZARAFA-REC-PATTERN":
                case "X-MOZ-SNOOZE-TIME":
                case "X-MOZ-SEND-INVITATIONS":
                    break;
                default:
                    ZLog::Write(LOGLEVEL_WARN, sprintf("BackendCalDAV->_ParseVEventToSyncObject(): '%s' is not yet supported.", $property->name));
            }
        }

        if ($message->meetingstatus > 0) {
            // No organizer was set for the meeting, assume it is the user
            if (!isset($message->organizeremail)) {
                $userDetails = ZPush::GetBackend()->GetCurrentUsername();
                $message->organizeremail = $userDetails['emailaddress'];
                $message->organizername = $userDetails['fullname'];
            }
            // Ensure the organizer name is set
            if (!isset($message->organizername)) {
                $message->organizername = Utils::GetLocalPartFromEmail($message->organizeremail);
            }
        }

        $message->timezone = $this->_GetTimezoneString($tzid);
        return $message;
    }

    /**
     * Parse a RRULE
     * @param string $rrulestr
     */
    private function _ParseRecurrence($rrulestr, $type) {
        $recurrence = new SyncRecurrence();
        if ($type == "vtodo") {
            $recurrence = new SyncTaskRecurrence();
        }
        $rrules = explode(";", $rrulestr);
        foreach ($rrules as $rrule) {
            $rule = explode("=", $rrule);
            switch ($rule[0]) {
                case "FREQ":
                    switch ($rule[1]) {
                        case "DAILY":
                            $recurrence->type = 0;
                            break;
                        case "WEEKLY":
                            $recurrence->type = 1;
                            break;
                        case "MONTHLY":
                            $recurrence->type = 2;
                            break;
                        case "YEARLY":
                            $recurrence->type = 5;
                    }
                    break;

                case "UNTIL":
                    $recurrence->until = strtotime($rule[1]);
                    break;

                case "COUNT":
                    $recurrence->occurrences = $rule[1];
                    break;

                case "INTERVAL":
                    $recurrence->interval = $rule[1];
                    break;

                case "BYDAY":
                    $dval = 0;
                    $days = explode(",", $rule[1]);
                    foreach ($days as $day) {
                        if ($recurrence->type == 2) {
                            if (strlen($day) > 2) {
                                $recurrence->weekofmonth = intval($day);
                                $day = substr($day,-2);
                            }
                            else {
                                $recurrence->weekofmonth = 1;
                            }
                            $recurrence->type = 3;
                        }
                        switch ($day) {
                            //   1 = Sunday
                            //   2 = Monday
                            //   4 = Tuesday
                            //   8 = Wednesday
                            //  16 = Thursday
                            //  32 = Friday
                            //  62 = Weekdays  // not in spec: daily weekday recurrence
                            //  64 = Saturday
                            case "SU":
                                $dval += 1;
                                break;
                            case "MO":
                                $dval += 2;
                                break;
                            case "TU":
                                $dval += 4;
                                break;
                            case "WE":
                                $dval += 8;
                                break;
                            case "TH":
                                $dval += 16;
                                break;
                            case "FR":
                                $dval += 32;
                                break;
                            case "SA":
                                $dval += 64;
                                break;
                        }
                    }
                    $recurrence->dayofweek = $dval;
                    break;

                    //Only 1 BYMONTHDAY is supported, so BYMONTHDAY=2,3 will only include 2
                case "BYMONTHDAY":
                    $days = explode(",", $rule[1]);
                    $recurrence->dayofmonth = $days[0];
                    break;

                case "BYMONTH":
                    $recurrence->monthofyear = $rule[1];
                    break;

                default:
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->_ParseRecurrence(): '%s' is not yet supported.", $rule[0]));
            }
        }
        return $recurrence;
    }

    /**
     * Generate a Sabre VCalendar from ActiveSync object.
     * @param string $data
     * @param string $folderid
     * @param string $id
     */
    private function _ParseASToVCalendar($data, $folderid, $id, $version=0) {
        $vcal = new Sabre\VObject\Component\VCalendar;
        $vcal->PRODID = "-//z-push-contrib//NONSGML Z-Push-contrib Calendar//EN";

        $tzid = defined('TIMEZONE') && strlen(TIMEZONE) > 1 ? TIMEZONE : 'UTC';
        if (isset($data->timezone) && isset($data->starttime)) {
            $tzid_tmp = $this->tzidFromMSTZ($data->timezone, $data->starttime);
            if($data->alldayevent != 1 || $tzid_tmp != 'UTC') {
                $tzid = $tzid_tmp;
            }
        }
        //Add VTIMEZONE
        $year = date("Y", $data->starttime);

        $vtimezone = $vcal->createComponent('VTIMEZONE');
        $vtimezone->TZID = $tzid;
        $timezone = new DateTimeZone($tzid);
        $transitions = $timezone->getTransitions(date("U", strtotime($year."0101T000000Z")), date("U", strtotime($year."1231T235959Z")));

        $offset_from = self::phpOffsetToIcalOffset($transitions[0]['offset']);
        for ($i=0; $i<count($transitions); $i++) {
            $offset_to = self::phpOffsetToIcalOffset($transitions[$i]['offset']);
            if ($i == 0) {
                $offset_from = $offset_to;
                if (count($transitions) > 1) {
                   continue;
                }
            }
            $vtransition = $vcal->createComponent($transitions[$i]['isdst'] == 1 ? "DAYLIGHT" : "STANDARD");

            $vtransition->TZOFFSETFROM = $offset_from;
            $vtransition->TZOFFSETTO = $offset_to;
            $offset_from = $offset_to;

            $vtransition->TZNAME = $transitions[$i]['abbr'];
            $vtransition->DTSTART = date("Ymd\THis", $transitions[$i]['ts']);
            $vtimezone->add($vtransition);
        }
        $vcal->add($vtimezone);

        switch ($folderid[0]){
            case "C":
                $vevent = $this->_ParseASEventToVEvent($vcal, $data, $tzid);
                $vevent->UID = $id;
                $vevent->SEQUENCE = $version;
                $exceptions = array();
                if (isset($data->exceptions) && is_array($data->exceptions)) {
                    foreach ($data->exceptions as $ex) {
                        if (isset($ex->deleted) && $ex->deleted == 1) {
                            if ($data->alldayevent == 1) {
                                $exdate = $this->_GetDateFromUTC("Ymd", $ex->exceptionstarttime, $tzid);
                            }
                            else {
                                $exdate = gmdate("Ymd\THis\Z", $ex->exceptionstarttime);
                            }
                            if (isset($vevent->EXDATE)) {
                                $vevent->EXDATE .= ",".$exdate;
                            }
                            else {
                                $vevent->EXDATE = $exdate;
                            }
                            continue;
                        }

                        $exception = $this->_ParseASEventToVEvent($vcal, $ex, $tzid);
                        $exception->UID = $id;
                        if ($data->alldayevent == 1) {
                            $exception->add('RECURRENCE-ID', $this->_GetDateFromUTC("Ymd", $ex->exceptionstarttime, $tzid), array("VALUE" => "DATE"));
                        }
                        else {
                            $exception->add('RECURRENCE-ID', gmdate("Ymd\THis\Z", $ex->exceptionstarttime));
                        }
                        $exceptions[] = $exception;
                    }
                }
                $vcal->add($vevent);
                foreach ($exceptions as $exception) {
                    $vcal->add($exception);
                }
                break;
            case "T":
                $vtodo = $this->_ParseASTaskToVTodo($vcal, $data);
                $vtodo->UID = $id;
                $vcal->add($vtodo);
                break;
        }

        ZLog::Write(LOGLEVEL_DEBUG, "VObject vcal: ".print_r($vcal->serialize(), 1));

        return $vcal->serialize();
    }

    /**
     * Generate a VEVENT from a SyncAppointment(Exception).
     * @param Sabre/VCalendar $vcal
     * @param string $data
     * @param string $tzid
     * @return VEVENT
     */
    private function _ParseASEventToVEvent($vcal, $data, $tzid) {
        $vevent = $vcal->createComponent('VEVENT');

        if (isset($data->dtstamp)) {
            $vevent->DTSTAMP = gmdate("Ymd\THis\Z", $data->dtstamp);
            $vevent->{'LAST-MODIFIED'} = gmdate("Ymd\THis\Z", $data->dtstamp);
        }
        else {
            $vevent->DTSTAMP = gmdate("Ymd\THis\Z");
        }
        if ($data->alldayevent == 1) {
            $vevent->{'X-MICROSOFT-CDO-ALLDAYEVENT'} = 'TRUE';
        }
        else {
            $vevent->{'X-MICROSOFT-CDO-ALLDAYEVENT'} = 'FALSE';
        }
        if (!isset($data->starttime)) {
            if (isset($data->endtime)) {
                $data->starttime = $data->endtime - 1800;
            }
            else {
                $data->starttime = time();
            }
        }
        if (!isset($data->endtime)) {
            $data->endtime = $data->starttime + 1800;
        }
        if ($data->alldayevent == 1) {
            $vevent->DTSTART = $this->_GetDateFromUTC("Ymd", $data->starttime, $tzid);
            $vevent->DTSTART['VALUE'] = 'DATE';
        }
        else {
            $vevent->DTSTART = $this->DAVDateTimeInTimezone($data->starttime, $tzid);
        }
        if ($tzid != "UTC") {
            $vevent->DTSTART['TZID'] = $tzid;
        }
        if ($data->alldayevent == 1) {
            $vevent->DTEND = $this->_GetDateFromUTC("Ymd", $data->endtime, $tzid);
            $vevent->DTEND['VALUE'] = 'DATE';
        }
        else {
            $vevent->DTEND = $this->DAVDateTimeInTimezone($data->endtime, $tzid);
        }
        if ($tzid != "UTC") {
            $vevent->DTEND['TZID'] = $tzid;
        }
        if (isset($data->subject)) {
            $vevent->SUMMARY = $data->subject;
        }
        if (isset($data->location)) {
            $vevent->LOCATION = $data->location;
        }
        if (isset($data->recurrence)) {
            $vevent->RRULE = $this->_GenerateRecurrence($data->recurrence, $data->alldayevent);
        }
        if (isset($data->sensitivity)) {
            switch ($data->sensitivity) {
                case 0:
                    $vevent->CLASS = "PUBLIC";
                    break;
                case 2:
                    $vevent->CLASS = "PRIVATE";
                    break;
                case 3:
                    $vevent->CLASS = "CONFIDENTIAL";
                    break;
            }
        }
        if (isset($data->busystatus)) {
            switch ($data->busystatus) {
                case 0: //Free
                    $vevent->TRANSP = "TRANSPARENT";
                    $vevent->{'X-MICROSOFT-CDO-INTENDEDSTATUS'} = "FREE";
                    break;
                case 1: //Tentative
                    $vevent->TRANSP = "OPAQUE";
                    $vevent->{'X-MICROSOFT-CDO-INTENDEDSTATUS'} = "TENTATIVE";
                    break;
                case 2: //Busy
                    $vevent->TRANSP = "OPAQUE";
                    $vevent->{'X-MICROSOFT-CDO-INTENDEDSTATUS'} = "BUSY";
                    break;
                case 3: //Out of office
                    $vevent->TRANSP = "TRANSPARENT";
                    $vevent->{'X-MICROSOFT-CDO-INTENDEDSTATUS'} = "OOF";
                    break;
                case 4: //Working elsewhere (not yet in Android)
                    $vevent->TRANSP = "TRANSPARENT";
                    $vevent->{'X-MICROSOFT-CDO-INTENDEDSTATUS'} = "WORKINGELSEWHERE";
                    break;
            }
        }
        if (isset($data->reminder)) {
            $valarm = $vcal->createComponent('VALARM');
            $valarm->add('ACTION', "DISPLAY");
            $valarm->add('DESCRIPTION', 'Default Z-Push description');
            $valarm->add('TRIGGER', "-PT".$data->reminder."M", ['RELATED' => "START"]);
            $vevent->add($valarm);
        }
        if (isset($data->rtf) && strlen($data->rtf) > 0) {
            $rtfparser = new z_RTF();
            $rtfparser->loadrtf(base64_decode($data->rtf));
            $rtfparser->output("ascii");
            $rtfparser->parse();
            $vevent->DESCRIPTION = $rtfparser->out;
        }
        if (isset($data->body) && strlen($data->body) > 0) {
            $vevent->DESCRIPTION = $data->body;
        }
        if (isset($data->asbody->data)) {
            $asbody = stream_get_contents($data->asbody->data);
            if (strlen($asbody) > 0) {
                $vevent->DESCRIPTION = $asbody;
            }
        }
        if (isset($data->meetingstatus) && $data->meetingstatus > 0) {
            switch ($data->meetingstatus) {
                //  0 = not a meeting
                case 1: // a meeting
                case 9: // as 1
                    $vevent->STATUS = "TENTATIVE";
                    $vevent->{'X-MICROSOFT-CDO-BUSYSTATUS'} = "TENTATIVE";
                    $vevent->{'X-MICROSOFT-DISALLOW-COUNTER'} = "FALSE";
                    break;
                case 3: // Meeting received
                case 11: // as 3
                    $vevent->STATUS = "CONFIRMED";
                    $vevent->{'X-MICROSOFT-CDO-BUSYSTATUS'} = "CONFIRMED";
                    $vevent->{'X-MICROSOFT-DISALLOW-COUNTER'} = "FALSE";
                    break;
                case 5: // Meeting is canceled
                case 13: // as 5
                case 7: // Meeting is canceled and received
                case 15: // as 7
                    $vevent->STATUS = "CANCELLED";
                    $vevent->{'X-MICROSOFT-CDO-BUSYSTATUS'} = "CANCELLED";
                    $vevent->{'X-MICROSOFT-DISALLOW-COUNTER'} = "TRUE";
                    break;
            }
            if (isset($data->organizeremail) && isset($data->organizername)) {
                $vevent->add('ORGANIZER', "mailto:".$data->organizeremail, ['CN' => $data->organizername]);
            }
            elseif (isset($data->organizeremail)) {
                $vevent->ORGANIZER = "mailto:".$data->organizeremail;
            }
            else {
                //Some phones doesn't send the organizeremail, so we gotto get it somewhere else.
                $userDetails = ZPush::GetBackend()->GetCurrentUsername();
                $vevent->add('ORGANIZER', "mailto:".$userDetails['emailaddress'], ['CN' => $userDetails['fullname']]);
            }
            if (isset($data->attendees) && is_array($data->attendees)) {
                foreach ($data->attendees as $att) {
                    $params = array();
                    if (isset($att->name) && strlen($att->name) > 0) {
                        $params['CN'] = $att->name;
                    }
                    if (isset($att->attendeestatus) && $att->attendeestatus > 0) {
                        switch ($att->attendeestatus) {
                            case 2: // Tentative
                                $params['PARTSTAT'] = "TENTATIVE";
                                break;
                            case 3: // Accept
                                $params['PARTSTAT'] = "ACCEPTED";
                                break;
                            case 4: // Decline
                                $params['PARTSTAT'] = "DECLINED";
                                break;
                            case 5: // Not responded
                                $params['PARTSTAT'] = "NEEDS-ACTION";
                                break;
                        }
                    }
                    if (isset($att->attendeetype)) {
                        switch ($att->attendeetype) {
                            case 1: // Required
                                $params['ROLE'] = "REQ-PARTICIPANT";
                                break;
                            case 2: // Optional
                                $params['ROLE'] = "OPT-PARTICIPANT";
                                break;
                            case 3: // Resource
                                $params['ROLE'] = "NON-PARTICIPANT";
                                break;
                        }
                    }
                    $vevent->add('ATTENDEE', "mailto:".$att->email, $params);
                }
            }
        }
        if (isset($data->categories) && is_array($data->categories)) {
            $vevent->CATEGORIES = implode(",", $data->categories);
        }
        return $vevent;
    }

    /**
     * Generate Recurrence
     * @param string $rec
     */
    private function _GenerateRecurrence($rec, $alldayevent = 0) {
        $rrule = array();
        if (isset($rec->type)) {
            $freq = "";
            switch ($rec->type) {
                case 0:
                    $freq = "DAILY";
                    break;
                case 1:
                    $freq = "WEEKLY";
                    break;
                case 2:
                case 3:
                    $freq = "MONTHLY";
                    break;
                case 5:
                    $freq = "YEARLY";
                    break;
            }
            $rrule[] = "FREQ=" . $freq;
        }
        if (isset($rec->occurrences)) {
            $rrule[] = "COUNT=" . $rec->occurrences;
        }
        if (isset($rec->until) && !isset($rec->occurrences)) {
            if ($alldayevent == 1) {
                $rrule[] = "UNTIL=" . gmdate("Ymd", $rec->until);
            }
            else {
                $rrule[] = "UNTIL=" . gmdate("Ymd\THis\Z", $rec->until);
            }
        }
        if (isset($rec->interval)) {
            $rrule[] = "INTERVAL=" . $rec->interval;
        }
        if (isset($rec->dayofweek)) {
            $week = '';
            if (isset($rec->weekofmonth)) {
                $week = $rec->weekofmonth;
            }
            $days = array();
            if (($rec->dayofweek & 1) == 1) {
                if (empty($week)) {
                    $days[] = "SU";
                }
                else {
                    $days[] = $week . "SU";
                }
            }
            if (($rec->dayofweek & 2) == 2) {
                if (empty($week)) {
                    $days[] = "MO";
                }
                else {
                    $days[] = $week . "MO";
                }
            }
            if (($rec->dayofweek & 4) == 4) {
                if (empty($week)) {
                    $days[] = "TU";
                }
                else {
                    $days[] = $week . "TU";
                }
            }
            if (($rec->dayofweek & 8) == 8) {
                if (empty($week)) {
                    $days[] = "WE";
                }
                else {
                    $days[] = $week . "WE";
                }
            }
            if (($rec->dayofweek & 16) == 16) {
                if (empty($week)) {
                    $days[] = "TH";
                }
                else {
                    $days[] = $week . "TH";
                }
            }
            if (($rec->dayofweek & 32) == 32) {
                if (empty($week)) {
                    $days[] = "FR";
                }
                else {
                    $days[] = $week . "FR";
                }
            }
            if (($rec->dayofweek & 64) == 64) {
                if (empty($week)) {
                    $days[] = "SA";
                }
                else {
                    $days[] = $week . "SA";
                }
            }
            $rrule[] = "BYDAY=" . implode(",", $days);
        }
        if (isset($rec->dayofmonth)) {
            $rrule[] = "BYMONTHDAY=" . $rec->dayofmonth;
        }
        if (isset($rec->monthofyear)) {
            $rrule[] = "BYMONTH=" . $rec->monthofyear;
        }
        return implode(";", $rrule);
    }

    /**
     * Convert a Sabre VTodo to ActiveSync format
     * @param string $data
     * @param ContentParameters $contentparameters
     */
    private function _ParseVTodoToAS($data, $contentparameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->_ParseVTodoToAS(): Parsing VTodo"));
        $truncsize = Utils::GetTruncSize($contentparameters->GetTruncation());

        $message = new SyncTask();
        $vcal = Sabre\VObject\Reader::read($data, Sabre\VObject\Reader::OPTION_FORGIVING);
        ZLog::Write(LOGLEVEL_DEBUG, "VObject vcal: ".print_r($vcal->serialize(), 1));

        foreach ($vcal->VTODO as $vtodo) {
            $message = $this->_ParseVTodoToSyncObject($vtodo, $message, $truncsize);
        }
        return $message;
    }

    /**
     * Parse 1 VEvent
     * @param Sabre/VTodo $vtodo
     * @param SyncAppointment(Exception) $message
     * @param int $truncsize
     */
    private function _ParseVTodoToSyncObject($vtodo, $message, $truncsize) {
        //Default
        $message->reminderset = 0;
        $message->importance = 1;
        $message->complete = 0;

        foreach ($vtodo->children() as $property) {
            switch ($property->name) {
                case "SUMMARY":
                    $message->subject = (string)$property;
                    break;

                case "STATUS":
                    switch ((string)$property) {
                        case "NEEDS-ACTION":
                        case "IN-PROCESS":
                            $message->complete = 0;
                            break;
                        case "COMPLETED":
                        case "CANCELLED":
                            $message->complete = 1;
                            break;
                    }
                    break;

                case "COMPLETED":
                    $message->datecompleted = $property->getDateTime()->getTimestamp();
                    break;

                case "DUE":
                    $message->utcduedate = $property->getDateTime()->getTimestamp();
                    break;

                case "PRIORITY":
                    $priority = (int)$property;
                    if ($priority <= 3)
                        $message->importance = 0;
                    if ($priority <= 6)
                        $message->importance = 1;
                    if ($priority > 6)
                        $message->importance = 2;
                    break;

                case "RRULE":
                    $message->recurrence = $this->_ParseRecurrence((string)$property, "vtodo");
                    break;

                case "CLASS":
                    switch ((string)$property) {
                        case "PUBLIC":
                            $message->sensitivity = 0;
                            break;
                        case "PRIVATE":
                            $message->sensitivity = 2;
                            break;
                        case "CONFIDENTIAL":
                            $message->sensitivity = 3;
                            break;
                    }
                    break;

                case "DTSTART":
                    $message->utcstartdate = $property->getDateTime()->getTimestamp();
                    break;

                case "SUMMARY":
                    $message->subject = (string)$property;
                    break;

                case "CATEGORIES":
                    $categories = explode(",", (string)$property);
                    $message->categories = $categories;
                    break;

                case "VALARM":
                    foreach ($property->children() as $valarm_child) {
                        if ($valarm_child->name == "TRIGGER") {
                            if (isset($valarm_child->parameters['VALUE']) && $valarm_child->parameters['VALUE'] == 'DATE-TIME') {
                                // the specific date and time is specified
                                $begin = date_create("@" . $vtodo->DTSTART->getDateTime()->getTimestamp());
                                $trigger = date_create("@" . TimezoneUtil::MakeUTCDate((string)$valarm_child));
                                $interval = date_diff($begin, $trigger);
                                $message->reminder = $interval->format("%i") + $interval->format("%h") * 60 + $interval->format("%a") * 60 * 24;
                            }
                            else {
                                // the trigger is relative
                                // ICAL allows the trigger to be relative to something other than the start, AS does not support this
                                $val = str_replace("-", "", (string)$valarm_child);
                                $interval = new DateInterval($val);
                                $message->reminder = $interval->format("%i") + $interval->format("%h") * 60 + $interval->format("%a") * 60 * 24;
                            }
                        }
                    }
                    break;
            }
        }
        if (isset($message->recurrence)) {
            $message->recurrence->start = $message->utcstartdate;
        }
        return $message;
    }

    /**
     * Generate a VTODO from a SyncAppointment(Exception)
     * @param Sabre\VCalendar $vcal
     * @param string $data
     * @return VTODO
     */
    private function _ParseASTaskToVTodo($vcal, $data) {
        $vtodo = $vcal->createComponent('VTODO');

        if (isset($data->dtstamp)) {
            $vtodo->DTSTAMP = gmdate("Ymd\THis\Z", $data->dtstamp);
            $vtodo->{'LAST-MODIFIED'} = gmdate("Ymd\THis\Z", $data->dtstamp);
        }
        else {
            $vtodo->DTSTAMP = gmdate("Ymd\THis\Z");
        }
        if (isset($data->subject)) {
            $vtodo->SUMMARY = $data->subject;
        }
        if (isset($data->rtf)) {
            $rtfparser = new z_RTF();
            $rtfparser->loadrtf(base64_decode($data->rtf));
            $rtfparser->output("ascii");
            $rtfparser->parse();
            $vtodo->DESCRIPTION = $rtfparser->out;
        }
        if (isset($data->body) && strlen($data->body) > 0) {
            $vtodo->DESCRIPTION = $data->body;
        }
        if (isset($data->asbody->data)) {
            $asbody = stream_get_contents($data->asbody->data);
            if (strlen($asbody) > 0) {
                $vtodo->DESCRIPTION = $asbody;
            }
        }

        if (isset($data->complete)) {
            if ($data->complete == 0) {
                $vtodo->STATUS = "NEEDS-ACTION";
            }
            else {
                $vtodo->STATUS = "COMPLETED";
            }
        }
        if (isset($data->datecompleted)) {
            $vtodo->COMPLETED = gmdate("Ymd\THis\Z", $data->datecompleted);
        }
        if ($data->utcduedate) {
            $vtodo->DUE = gmdate("Ymd\THis\Z", $data->utcduedate);
        }
        if (isset($data->importance)) {
            if ($data->importance == 1) {
                $vtodo->PRIORITY = 6;
            }
            elseif ($data->importance == 2) {
                $vtodo->PRIORITY = 9;
            }
            else {
                $vtodo->PRIORITY = 1;
            }
        }
        if (isset($data->recurrence)) {
            $vtodo->RRULE = $this->_GenerateRecurrence($data->recurrence);
        }
        if ($data->reminderset && $data->remindertime) {
            $valarm = $vcal->createComponent('VALARM');
            $valarm->add('ACTION', "DISPLAY");
            $valarm->add('DESCRIPTION', 'Default Z-Push description');
            $valarm->add('TRIGGER', gmdate("Ymd\THis\Z", $data->remindertime), ['VALUE' => "DATE-TIME"]);
            $vtodo->add($valarm);
        }
        if (isset($data->sensitivity)) {
            switch ($data->sensitivity) {
                case 0:
                    $vtodo->CLASS = "PUBLIC";
                    break;
                case 2:
                    $vtodo->CLASS = "PRIVATE";
                    break;
                case 3:
                    $vtodo->CLASS = "CONFIDENTIAL";
                    break;
            }
        }
        if (isset($data->utcstartdate)) {
            $vtodo->DTSTART = gmdate("Ymd\THis\Z", $data->utcstartdate);
        }
        if (isset($data->categories) && is_array($data->categories)) {
            $vtodo->CATEGORIES = implode(",", $data->categories);
        }

        return $vtodo;
    }

    private function _GetDateFromUTC($format, $date, $tzid) {
        $dt = date_create('@' . $date);
        date_timezone_set($dt, timezone_open($tzid));
        return date_format($dt, $format);
    }

    /**
     * Generate ActiveSync Timezone Packed String.
     * @param string $timezone
     * @param string $with_names
     * @throws Exception
     */
    private function _GetTimezoneString($timezone, $with_names = true) {
        // UTC needs special handling
        if ($timezone == "UTC")
            return base64_encode(pack('la64vvvvvvvvla64vvvvvvvvl', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0));
        try {
            //Generate a timezone string (PHP 5.3 needed for this)
            $timezone = new DateTimeZone($timezone);
            $trans = $timezone->getTransitions(date('U',strtotime(date('Y-01-01'))), date('U',strtotime(date('Y-12-31'))));
            $stdTime = null;
            $dstTime = null;
            if (count($trans) < 1) {
                throw new Exception();
            }
            if (count($trans) < 3) {
                $stdBias = $trans[count($trans)-1]['offset'] / -60;
                $stdName = $trans[count($trans)-1]['abbr'];
                $stdMonth = 0;
                $stdDay = 0;
                $stdWeek = 0;
                $stdHour = 0;
                $stdMinute = 0;
                $dstName = "";
                $dstMonth = 0;
                $dstDay = 0;
                $dstWeek = 0;
                $dstHour = 0;
                $dstMinute = 0;
                $dstBias = -60;
            }
            else {
                if ($trans[1]['isdst'] == 1) {
                    $dstTime = $trans[1];
                    $stdTime = $trans[2];
                }
                else {
                    $dstTime = $trans[2];
                    $stdTime = $trans[1];
                }
                $stdTimeO = new DateTime($stdTime['time'], timezone_open("UTC"));
                $stdTimeO->add(new DateInterval('PT'.$dstTime['offset'].'S'));
                $stdFirst = new DateTime(sprintf("first sun of %s %s", $stdTimeO->format('F'), $stdTimeO->format('Y')), timezone_open("UTC"));
                $stdBias = $stdTime['offset'] / -60;
                $stdName = $stdTime['abbr'];
                $stdYear = 0;
                $stdMonth = $stdTimeO->format('n');
                $stdWeek = floor(($stdTimeO->format("j")-$stdFirst->format("j"))/7)+1;
                $stdDay = $stdTimeO->format('w');
                $stdHour = $stdTimeO->format('H');
                $stdMinute = $stdTimeO->format('i');
                $stdTimeO->add(new DateInterval('P7D'));
                if ($stdTimeO->format('n') != $stdMonth) {
                    $stdWeek = 5;
                }
                $dstTimeO = new DateTime($dstTime['time'], timezone_open("UTC"));
                $dstTimeO->add(new DateInterval('PT'.$stdTime['offset'].'S'));
                $dstFirst = new DateTime(sprintf("first sun of %s %s", $dstTimeO->format('F'), $dstTimeO->format('Y')), timezone_open("UTC"));
                $dstName = $dstTime['abbr'];
                $dstYear = 0;
                $dstMonth = $dstTimeO->format('n');
                $dstWeek = floor(($dstTimeO->format("j")-$dstFirst->format("j"))/7)+1;
                $dstDay = $dstTimeO->format('w');
                $dstHour = $dstTimeO->format('H');
                $dstMinute = $dstTimeO->format('i');
                $dstTimeO->add(new DateInterval('P7D'));
                if ($dstTimeO->format('n') != $dstMonth) {
                    $dstWeek = 5;
                }
                $dstBias = ($dstTime['offset'] - $stdTime['offset']) / -60;
            }
            if ($with_names) {
                return base64_encode(pack('la64vvvvvvvvla64vvvvvvvvl', $stdBias, $stdName, 0, $stdMonth, $stdDay, $stdWeek, $stdHour, $stdMinute, 0, 0, 0, $dstName, 0, $dstMonth, $dstDay, $dstWeek, $dstHour, $dstMinute, 0, 0, $dstBias));
            }
            else {
                return base64_encode(pack('la64vvvvvvvvla64vvvvvvvvl', $stdBias, '', 0, $stdMonth, $stdDay, $stdWeek, $stdHour, $stdMinute, 0, 0, 0, '', 0, $dstMonth, $dstDay, $dstWeek, $dstHour, $dstMinute, 0, 0, $dstBias));
            }
        }
        catch (Exception $e) {
            // If invalid timezone is given, we return UTC
            return base64_encode(pack('la64vvvvvvvvla64vvvvvvvvl', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0));
        }
        return base64_encode(pack('la64vvvvvvvvla64vvvvvvvvl', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0));
    }

    private static $knownMSTZS = array(
        "0/0/0/0/0/0/0/0/0/0"=>"UTC"
        ,"-780/-60/0/0/0/0/0/0/0/0"=>"Pacific/Enderbury"
        ,"-720/-60/4/1/0/3/9/5/0/2"=>"Pacific/Auckland"
        ,"-660/-60/0/0/0/0/0/0/0/0"=>"Antarctica/Casey"
        ,"-600/-60/4/1/0/3/10/1/0/2"=>"Australia/Melbourne"
        ,"-600/-60/0/0/0/0/0/0/0/0"=>"Australia/Brisbane"
        ,"-570/-60/4/1/0/3/10/1/0/2"=>"Australia/Adelaide"
        ,"-570/-60/0/0/0/0/0/0/0/0"=>"Australia/Darwin"
        ,"-540/-60/0/0/0/0/0/0/0/0"=>"Asia/Chita"
        ,"-480/-60/0/0/0/0/0/0/0/0"=>"Asia/Brunei"
        ,"-420/-60/0/0/0/0/0/0/0/0"=>"Antarctica/Davis"
        ,"-390/-60/0/0/0/0/0/0/0/0"=>"Asia/Yangon"
        ,"-360/-60/0/0/0/0/0/0/0/0"=>"Antarctica/Vostok"
        ,"-345/-60/0/0/0/0/0/0/0/0"=>"Asia/Kathmandu"
        ,"-330/-60/0/0/0/0/0/0/0/0"=>"Asia/Colombo"
        ,"-300/-60/0/0/0/0/0/0/0/0"=>"Antarctica/Mawson"
        ,"-270/-60/0/0/0/0/0/0/0/0"=>"Asia/Kabul"
        ,"-210/-60/9/3/4/22/3/3/3/22"=>"Asia/Tehran"
        ,"-180/-60/0/0/0/0/0/0/0/0"=>"Africa/Addis_Ababa"
        ,"-120/-60/10/5/0/4/3/5/0/3"=>"Europe/Helsinki"
        ,"-120/-60/0/0/0/0/0/0/0/0"=>"Africa/Blantyre"
        ,"-60/-60/10/5/0/3/3/5/0/2"=>"Europe/Berlin"
        ,"-60/-60/10/4/0/3/3/5/0/2"=>"Europe/Berlin"
        ,"-60/-60/0/0/0/0/0/0/0/0"=>"Africa/Algiers"
        ,"0/-60/10/5/0/2/3/5/0/1"=>"Europe/Dublin"
        ,"0/-60/10/4/0/2/3/5/0/1"=>"Europe/Dublin"
        ,"0/-60/0/0/0/0/0/0/0/0"=>"Africa/Abidjan"
        ,"60/-60/0/0/0/0/0/0/0/0"=>"Atlantic/Cape_Verde"
        ,"180/-60/2/4/6/23/10/3/6/23"=>"America/Sao_Paulo"
        ,"180/-60/10/5/6/23/3/4/6/22"=>"America/Godthab"
        ,"180/-60/0/0/0/0/0/0/0/0"=>"America/Araguaina"
        ,"240/-60/11/1/0/2/3/2/0/2"=>"America/Barbados"
        ,"240/-60/0/0/0/0/0/0/0/0"=>"America/Anguilla"
        ,"270/-60/0/0/0/0/0/0/0/0"=>"America/Caracas"
        ,"300/-60/11/1/0/2/3/2/0/2"=>"America/New_York"
        ,"300/-60/0/0/0/0/0/0/0/0"=>"America/Atikokan"
        ,"360/-60/11/1/0/2/3/2/0/2"=>"America/Chicago"
        ,"360/-60/0/0/0/0/0/0/0/0"=>"America/Belize"
        ,"420/-60/10/5/0/2/4/1/0/2"=>"America/Chihuahua"
        ,"420/-60/11/1/0/2/3/2/0/2"=>"America/Denver"
        ,"420/-60/0/0/0/0/0/0/0/0"=>"America/Creston"
        ,"480/-60/11/1/0/2/3/2/0/2"=>"America/Los_Angeles"
        ,"540/-60/11/1/0/2/3/2/0/2"=>"America/Anchorage"
        ,"600/-60/0/0/0/0/0/0/0/0"=>"Pacific/Honolulu"
    );

    /**
     * Given the MS timezone find a matching tzid, for the year the event starts in.
     * @param string $mstz
     * @param string $eventstart
     * @return string
     */
    public static function tzidFromMSTZ($mstz, $eventstart){
        // 1. Check known MS time zones
        $mstz_parts = unpack("lbias/a64tzname/vdstendyear/vdstendmonth/vdstendday/vdstendweek/vdstendhour/"
                                    ."vdstendminute/vdstendsecond/vdstendmillis/lstdbias/a64tznamedst/vdststartyear/"
                                    ."vdststartmonth/vdststartday/vdststartweek/vdststarthour/vdststartminute/"
                                    ."vdststartsecond/vdststartmillis/ldstbias", base64_decode($mstz));
        $mstz_parts['tzname'] = trim(mb_convert_encoding($mstz_parts['tzname'], "ASCII" , "UTF-16LE"));
        $mstz_parts['tznamedst'] = trim(mb_convert_encoding($mstz_parts['tznamedst'], "ASCII" , "UTF-16LE"));

        if (isset(self::WINDOWSTOSTANDARDTZ[$mstz_parts['tzname']]))
            return self::WINDOWSTOSTANDARDTZ[$mstz_parts['tzname']];
        if (isset(self::WINDOWSTOSTANDARDTZ[$mstz_parts['tznamedst']]))
            return self::WINDOWSTOSTANDARDTZ[$mstz_parts['tznamedst']];

        $mstz = $mstz_parts['bias']
                    ."/".$mstz_parts['dstbias']
                    ."/".$mstz_parts['dstendmonth']
                    ."/".$mstz_parts['dstendweek']
                    ."/".$mstz_parts['dstendday']
                    ."/".$mstz_parts['dstendhour']
                    ."/".$mstz_parts['dststartmonth']
                    ."/".$mstz_parts['dststartweek']
                    ."/".$mstz_parts['dststartday']
                    ."/".$mstz_parts['dststarthour'];
        if (isset(self::$knownMSTZS[$mstz])) {
            $tzid = self::$knownMSTZS[$mstz];
            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Found tzid in known list: '%s'.", $tzid));
            return $tzid;
        }

        // 2. Loop all time zones to find a match on offset and transition date
        $year = date("Y", $eventstart);
        $offset_std = -($mstz_parts["bias"] * 60);
        $offset_dst = -(($mstz_parts["bias"] + $mstz_parts["dstbias"]) * 60);
        $dststart_timestamp = self::timestampFromMSTZ($mstz_parts, "dststart", $mstz_parts["bias"], $year);
        $dstend_timestamp = self::timestampFromMSTZ($mstz_parts, "dstend", $mstz_parts["bias"] + $mstz_parts["dstbias"], $year);

        $tzids = DateTimeZone::listIdentifiers();
        foreach ($tzids as $tzid) {
            $timezone = new DateTimeZone($tzid);
            $transitions = $timezone->getTransitions(date("U", strtotime($year."0101T000000Z")), date("U", strtotime($year."1231T235959Z")));

            $tno = count($transitions);
            if ($tno == 1 && $dststart_timestamp == 0) {
                if ($transitions[0]['offset'] == $offset_std) {
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Found tzid: '%s'.", $tzid));
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Add tzid to knownMSTZS array for better performance: '%s'.", ',"'.$mstz.'"=>"'.$tzid.'"'));
                    return $tzid;
                }
            }
            else if (($tno == 3 || $tno == 5) && $dststart_timestamp != 0) {
                if ($dststart_timestamp < $dstend_timestamp) {
                    if(
                        $transitions[1]['isdst'] == 1 &&
                        $transitions[1]['ts'] == $dststart_timestamp &&
                        $transitions[1]['offset'] == $offset_dst &&
                        $transitions[2]['isdst'] == 0 &&
                        $transitions[2]['ts'] == $dstend_timestamp &&
                        $transitions[2]['offset'] == $offset_std)
                    {
                        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Found tzid: '%s'.", $tzid));
                        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Add tzid to knownMSTZS array for better performance: '%s'.", ',"'.$mstz.'"=>"'.$tzid.'"'));
                        return $tzid;
                    }
                }
                else {
                    if (
                        $transitions[1]['isdst'] == 0 &&
                        $transitions[1]['ts'] == $dstend_timestamp &&
                        $transitions[1]['offset'] == $offset_std &&
                        $transitions[2]['isdst'] == 1 &&
                        $transitions[2]['ts'] == $dststart_timestamp &&
                        $transitions[2]['offset'] == $offset_dst)
                    {
                        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Found tzid: '%s'.", $tzid));
                        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendCalDAV->tzidFromMSTZ(): Add tzid to knownMSTZS array for better performance: '%s'.", ',"'.$mstz.'"=>"'.$tzid.'"'));
                        return $tzid;
                    }
                }
            }
        }

        // 3. Give up, use UTC
        ZLog::Write(LOGLEVEL_WARN, sprintf("BackendCalDAV->tzidFromMSTZ(): Failed to find tzid, defaulting to UTC. MS time zone: '%s'.", join('/', $mstz_parts)));
        return null;
    }

    /*
     * Calculate a unix timestamp for DST or STD start times in the MS time zone.
     * @param array $mstz_parts
     * @param string $prefix
     * @param integer $bias
     * @param string $year
     * @return integer
     */
    private static function timestampFromMSTZ($mstz_parts, $prefix, $bias, $year){
        if($mstz_parts[$prefix."month"] == 0){return 0;} // If month is empty, there is no transition

        $month = $mstz_parts[$prefix."month"];
        $weeks = array('', 'first', 'second', 'third', 'fourth', 'last');
        $week = $weeks[$mstz_parts[$prefix."week"]];
        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
        $day = $days[$mstz_parts[$prefix."day"]];

        $second = $mstz_parts[$prefix."hour"] * 3600 + $mstz_parts[$prefix."minute"] * 60 + $mstz_parts[$prefix."second"] + $bias * 60;

        return date("U", strtotime("$week $day of $year-$month Z") + $second);
    }

    /*
     * Calculate a DAV datetime field with timezone
     * @param int $timestamp
     * @param string $tzid
     * @return string
     */
    private static function DAVDateTimeInTimezone($timestamp, $tzid) {
        $dt = new DateTime('@'.$timestamp);
        $dt->setTimeZone(new DateTimeZone($tzid));
        return $tzid == 'UTC' ? $dt->format('Ymd\THis\Z') : $dt->format('Ymd\THis');
    }

    /**
     * Convert the offset provided by php to what ical uses in VTIMEZONE.
     * @param integer $phpoffset
     * @return string
     */
    private static function phpOffsetToIcalOffset($phpoffset) {
        $prefix = $phpoffset < 0 ? "-" : "+";
        $offset = abs($phpoffset);
        $hours = floor($offset / 3600);
        return sprintf("$prefix%'.02d%'.02d", $hours, ($offset - ($hours * 3600)) / 60);
    }

};
